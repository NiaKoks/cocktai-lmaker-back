const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const cocktails = require('./app/cocktails');
const users = require('./app/users');
const mycocktails = require('./app/mycocktails');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/CocktailMaker',{useNewUrlParser: true}).then(()=>{
    app.use('/cocktails',cocktails);
    app.use('/mycocktails',mycocktails);
    app.use('/users',users);

    app.listen(port,()=> console.log(`Server runned on ${port} port`));
});