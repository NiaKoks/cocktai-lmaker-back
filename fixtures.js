const nanoid = require ("nanoid");
const mongoose = require('mongoose');

const Cocktail =require('./models/Cocktail');
const User =require('./models/User');
const config = require('./config');

const run = async () =>{
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();


    for(let collection of collections){
        await collection.drop();

    }

    const [user,admin] = await User.create(
        {   username:'Test User',
            role: 'user',
            token: nanoid(),
            displayName:'TestUSER',
            facebookId: '102116221039848',
            avatar: null
        },
        {   username:'Admin',
            role: 'admin',
            token: nanoid(),
            displayName:'Admin',
            facebookId: '110726613502619',
            avatar: 'freddie.jpg'
        }
    );


 await Cocktail.create(

        {   user:user._id,
            name: 'Pina Colada',
            image:'one-pina-colada.jpeg',
            recepie:'mix vodka an pinapple juice with some coconut milk.Enjoy!',
            published: true,
            ingredients: [
                {name:'vodka',ammount:'15ml'},
                {name:'pineapple juice',ammount:'15ml'},
                {name:'coconut milk',ammount:'15ml'}],
        },
        {   user:admin._id,
            name: 'Double Pina Colada',
            image:'two-pina-colada.jpeg',
            recepie:'mix vodka an pinapple juice with some coconut milk.Enjoy!',
            published: false,
            ingredients: [{name:'vodka',ammount:'15ml'},
                {name:'pineapple juice',ammount:'15ml'},
                {name:'coconut milk',ammount:'15ml'}
            ],
        },
       {   user: user._id,
           name: 'Triple Pina Colada',
           image:'Three-Pina-Colada.jpg',
            recepie:'mix vodka an pineapple juice with some coconut milk.Enjoy!',
           published: true,
           ingredients: [{name:'vodka',ammount:'15ml'},
                         {name:'pineapple juice',ammount:'15ml'},
                         {name:'coconut milk',ammount:'15ml'}
                        ],
       },
        {   user:user._id,
            name: 'Forth Pina Colada',
            image:'forth-pina-colada.jpg',
            recepie:'mix vodka an pinapple juice with some coconut milk.Enjoy!',
            published: true,
            ingredients: [{name:'vodka',ammount:'15ml'},
                {name:'pineapple juice',ammount:'15ml'},
                {name:'coconut milk',ammount:'15ml'}
            ],
        }
   );

    return connection.close();
};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});
