const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name:{
        type: String,
        required: true,
    },
    image: String,
    recepie: {
        type: String,
        required: true
    },
    published:{
      type:Boolean,
      default: false
    },
    ingredients:[
        {
            name: String,
            ammount: String
        }
    ],
    raiting:[
        {
            userID: String,
            score: Number
        }
    ]
});
const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;