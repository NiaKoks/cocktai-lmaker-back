const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/CocktailMaker',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true},
    facebook: {appId:'2521606674646355',appSecret:'931b03230eef05046478a83361f419be'}
};