const mongoose = require('mongoose');
const nanoid = require('nanoid');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username:{
        type:String,
        unique: true,
        required: true
    },
    role:{
        type:String,
        required:true,
        default:'user',
        enum: ['admin','user']
    },
    avatar: String,
    displayName:{
        type: String,
        required: true
    },
    token:{
        type: String,
        required: true
    },
    avatar:String,
    facebookId: String
});

UserSchema.methods.generateToken = function() {
    this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;