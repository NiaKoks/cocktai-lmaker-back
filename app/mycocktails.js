const express = require('express');
const User = require ('../models/User');
const Cocktail = require('../models/Cocktail');

const auth = require('../middleware/auth');

const router = express.Router();

router.post('/' ,auth,async (req,res)=>{

    const cocktail = new Cocktail({
        cocktail: req.body.cocktailID,
        user: req.user._id
    });

    cocktail.save()
        .then(result => res.sendStatus(200))
        .catch((e) => res.status(500).send(e));
});

router.get('/', auth,(req,res)=>{
    Cocktail.find({user : req.user.id}).populate('cocktail')
        .then(cocktails => res.send(cocktails))
        .catch(()=>res.sendStatus(500))
});
module.exports = router;