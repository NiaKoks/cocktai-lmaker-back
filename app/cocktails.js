const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');
const Cocktail = require('../models/Cocktail');

const router = express.Router();

const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/',[tryAuth],(req,res)=>{

    let criteria = {published: true};

    if (req.user && req.user.role === 'admin'){
        criteria = {};
    }
    if (req.query.cocktail) {
        criteria.cocktail = req.query.cocktail;
    }
    Cocktail.find(criteria)
        .then(cocktails => res.send(cocktails))
        .catch(()=> res.sendStatus(500));
});
router.post('/',[auth, upload.single('image')],(req,res)=>{
    let image = 'notFound.png';
    if (req.file){
        image = req.file.fieldname;
    }
    let ingredients = [];

    try {
        ingredients = JSON.parse(req.body.ingredients);
    } catch (e) {
        return res.status(500).send('incorrect ingrediets array');
    }

    const coctalData = {
        user: req.user._id,
        name: req.body.name,
        image,
        recepie: req.body.recepie,
        ingredients,
    };

    const cocktail = new Cocktail(coctalData);
    cocktail.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/published',[auth,permit('admin')],async (req,res) =>{
    try{
        const cocktail = await Cocktail.findById(req.params.id)
        if(!cocktail) {res.sendStatus(404)}
        cocktail.published = req.body.state;
        await cocktail.save();
    } catch (e) {
        return res.status(500).send(e)
    }
    res.sendStatus(200)
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{

    Cocktail.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;